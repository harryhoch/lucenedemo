package edu.pitt.dbmi.deepphe;


import java.lang.Exception;
import java.lang.System;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Set;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.FieldInfos;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.index.SlowCompositeReaderWrapper;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;


/**
 * Hello world!
 *
 */
public class LuceneDemo   {


    public static boolean verbose =false;
   // static String lucenePath="/Users/harry/Documents/research/projects/deeppheno/dev/v2-3.5/output/output_graph/deepphe.db/index/lucene/node/name_index";

    //static String lucenePath="/Users/harry/Documents/research/projects/deepphe/v2/output/output_graph/deepphe.db/index/lucene/node/name_index";

    //static String lucenePath="/Users/harry/Documents/research/projects/deepphe/lucene-5.5.5/dist/lucene-5.5.5/index";

    static String lucenePath="/Users/harry/Documents/research/projects/deeppheno/dev/lucene5.5/dist/lucene-5.5.5/index";

    public static void main( String[] args ) throws Exception {
        Path currentRelativePath = Paths.get("");
        String cwd = currentRelativePath.toAbsolutePath().toString();
        System.out.println("directory is " + cwd);
        System.out.println("index is "+lucenePath);

        // open the search
        //in directory
        // /Users/harry/Documents/research/projects/deeppheno/dev/v2-3.5/output/output_graph/deepphe.db/index/lucene/node/name_index

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(lucenePath)));

        getTerms(reader);

        getContexts(reader);


        getDocCount(reader,"contents");
        getDocCount(reader,"notes");


        getLeaves(reader);

        // get field names.      https://stackoverflow.com/questions/20980466/lucene-reading-all-field-names-that-are-stored
        getFields(reader);

        System.out.println("# of documents "+reader.maxDoc());

        if (verbose == true) {
            showDocs(reader);
        }
        doSearch(reader);


    }

    public static void getTerms(IndexReader reader) throws Exception {

        Terms terms = SlowCompositeReaderWrapper.wrap(reader).terms("field");
        if (terms == null) {
            System.out.println("no terms");
        }
        else {
            TermsEnum tnum = terms.iterator();
            BytesRef t;
            while ((t = tnum.next()) != null) {
                String tString = t.toString();
                System.out.println(tString);
            }
        }
    }


    public static void doSearch(IndexReader reader) throws Exception {

        System.out.println("-=-=-=-=-=-=-=-=-=");
        System.out.println("running a search.. ");
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = new StandardAnalyzer();

        // created analyzer. now, let's run a search.
        QueryParser parser = new QueryParser("_id_",analyzer);

        Query query = parser.parse("adenoma");
        TopDocs results = searcher.search(query,100);

        int numTotalHits= results.totalHits;
        System.out.println("# of results..."+numTotalHits);

        System.out.println("-=-=-=-=-=-=-=-=-=");
    }


    public static void getFields(IndexReader reader) throws Exception {

        HashMap<String,Integer> allFields = new HashMap<String,Integer>();


        int maxdoc = reader.maxDoc();
        for (int i =0; i <maxdoc; i++) {
            Document doc = reader.document(i);
            List<IndexableField> fields = doc.getFields();
            for (IndexableField f : fields) {
                String field = f.name();
                if (!allFields.containsKey(field)) {
                    allFields.put(field, 0);
                }
                int count = allFields.get(field) + 1;
                allFields.put(field, count);
            }
        }
        System.out.println("indexed fields...");
        Set<String> foundFields = allFields.keySet();
        for (String s: foundFields) {
            System.out.println(s);
        }
        System.out.println("done with indexed fields");
    }

    public static void showDocs(IndexReader reader) throws Exception {
        for (int i =0; i < reader.maxDoc(); i++) {
            showDoc(reader,i);
        }

    }

    public static void showDoc(IndexReader reader,int i) throws Exception {
        Document doc = reader.document(i);
        System.out.println("=======");
        System.out.println("Document "+i);
        System.out.println("name is "+doc.get("name"));
        System.out.println(doc.toString());
        List<IndexableField> fields = doc.getFields();
        for (IndexableField f : fields) {
            String field = f.name();
            System.out.println("Field: "+field);
            String [] fvals = doc.getValues(field);
            for (String fv: fvals) {
                System.out.println("..."+fv);
            }
        }
    }

    public static void getDocCount(IndexReader ir,String field) throws Exception {
        int count = ir.getDocCount(field);
        System.out.println("document count for '"+field+"' is "+count);
    }

    public static void getLeaves(IndexReader reader) throws Exception {

        System.out.println("..........");
        System.out.println("leaf field names..");

        HashMap<String,Integer> allFields = new HashMap<String,Integer>();

        for (LeafReaderContext rc: reader.leaves()) {
            LeafReader ar = rc.reader();
            FieldInfos fis = ar.getFieldInfos();
            for (FieldInfo fi: fis) {
                String name = fi.name;
                if(!allFields.containsKey(name)) {
                    allFields.put(name,0);
                }
                int count = allFields.get(name)+1;
                allFields.put(name,count);
            }
        }

        Set<String> fnames = allFields.keySet();
        for (String s: fnames) {
            System.out.println(s);
        }
        System.out.println("..........");
    }


    static void getContexts(IndexReader reader) throws Exception {
        IndexReaderContext context = reader.getContext();
        System.out.println("*+*+*+*+*+*+");
        ArrayDeque<IndexReaderContext> contexts = new ArrayDeque<IndexReaderContext>();
        contexts.add(context);

       while (!contexts.isEmpty()) {
            IndexReaderContext irc = contexts.poll();
           if (irc.isTopLevel == true) {
                System.out.println("top level...");
            }  else {
                System.out.println("not top level");
            }
           List<IndexReaderContext> children = irc.children();
           if (children != null) {
               System.out.println("adding children");
               for (IndexReaderContext c : children) {
                   contexts.add(c);
               }
           }
        }
        System.out.println("*+*+*+*+*+*+");
    }
}